import React, {useContext, useState, useEffect} from 'react'
import styled from '@emotion/styled'
import { AuthContext } from '../Components/User/AuthContext';
import { useNavigate } from 'react-router-dom';
import Input from '../Components/Form/Input';
import Submit from '../Components/Form/Submit';
import Label from '../Components/Form/Label';
import TextArea from '../Components/Form/TextArea'
import { TwitterPicker } from 'react-color';
import axios from 'axios';
import { css } from '@emotion/css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {BsTwitter, BsFacebook, BsInstagram} from 'react-icons/bs';
import PokemonCard from '../Components/Cards/PokemonCard';
import useUpdateAuth from '../Querys/updateAuthQuery';
import useUpdateUser from '../Querys/updateUserQuery';
import { useShowEmail } from '../Querys/showEmailQuery';
import { useShowSocials } from '../Querys/showSocialsQuery';
import { useShowDob } from '../Querys/showDobQuery';
import { useShowFullname } from '../Querys/showFullnameQuery';
import { useShowBio } from '../Querys/showBioQuery';
import { useShowGender } from '../Querys/showGenderQuery';
import { useShowInterests } from '../Querys/showInterestsQuery';

const PageWrapper = styled.div`
    height:100%;
    width:100%;
    overflow-x:hidden;
`

const Wrapper = styled.form`
    height:100%;
    width:80%;
    margin-left:10%;
    margin-top:2.5%;
    margin-bottom:70px;
    display:flex;
    flex-direction:row;
    flex-wrap:wrap;
    position:relative;
    @media (max-width: 770px) {
        width:100%;
        margin-left:0;
    }
`

const LabelGroup = styled.div`
    min-height:50px;
    width:42%;
    margin:1% 2% 1% 2%;
    padding:2%;
    border-radius:15px;
    background:#FFD57B;
    @media (max-width: 770px) {
        width:96%;
    }
`

const DisplayText = styled.span`
    min-height:35px;
    width:100%;
    margin:3% 2% 2% 3%;
    padding:2.5px;
`

const Error = styled.span`
    font-size:0.8em;
    color:#F44336;
    margin-left:2.5%;
`

const Select = styled.select`
    border:none;
    height:30px;
    padding:5px;
    margin-left:2.5%;
    margin-top:5px;
    margin-bottom:5px;
`

function EditProfile() {
  const { isLoggedIn, username, pokemon } = useContext(AuthContext);
  const [updatedPassword, setUpdatedPassword] = useState('');
  const [passwordRetype, setPasswordRetype] = useState('');
  const [updateFullName, setUpdateFullName] = useState('');
  const [bio, setBio] = useState('');
  const [profileIconColor, setProfileIconColor] = useState('#3F51B5'); // Default color
  const [gender, setGender] = useState('');
  const [isLoading, setLoading] = useState(true);
  const [socialMedia, setSocialMedia] = useState({
    twitter: "",
    facebook: "",
    instagram: "",
  });
  const [interests, setInterests] = useState([]);
  const [isLoadingUsername, setLoadingUsername] = useState(true);
  const [errors, setErrors] = useState({});
  const navigate = useNavigate();
  const addUserMutation = useUpdateUser();
  const addAuthMutation = useUpdateAuth();

  const { data: currentEmail } = useShowEmail(username);
  const { data: socials } = useShowSocials(username);
  const { data: currentDob } = useShowDob(username);
  const { data: fullname } = useShowFullname(username);
  const { data: currentBio } = useShowBio(username);
  const { data: curGender } = useShowGender(username);
  const { data: currentInterests } = useShowInterests(username);


  useEffect(() => {
    if (username !== null) {
      if((currentEmail || socials || currentDob || fullname || currentBio || curGender || currentInterests) !== undefined){
        setLoading(false)
      }
    } else {
      setLoadingUsername(false); // Set loading to false if username is not found
    }
  }, [username, currentEmail, socials, currentDob, currentBio, fullname, curGender, currentInterests]);

  const handleUpdate = async (event) => {
    event.preventDefault();
    const formErrors = {};

    //Check to see if a correct 'Full Name' is entered
    if (updateFullName.trim() === '') {
        formErrors.updateFullName = 'Full Name is required';
    } else if (!/^[a-zA-Z ]+$/.test(updateFullName)) {
        formErrors.updateFullName = 'Full Name can only contain letters and spaces';
    }

    //Check to see if a correct 'Password' is entered and matched correctly
    if (updatedPassword.trim() === '') {
        formErrors.updatedPassword = 'Password is required';
    } else if (updatedPassword.length < 6) {
        formErrors.updatedPassword = 'Password must be at least 6 characters long';
    } else if (
        !/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*().]).{6,}/.test(updatedPassword)
    ) {
        formErrors.updatedPassword =
        'Password must contain at least one uppercase letter, one lowercase letter, one number, and one symbol';
    }

    if (updatedPassword !== passwordRetype) {
        formErrors.passwordRetype = 'Passwords do not match';
    }

    setErrors(formErrors);

    const authUpdateData = {
        username,
        updatedPassword,
        updateFullName,
    };

    const userUpdateData = {
        username,
        profileIconColor,
        bio,
        gender,
        interests,
        socialMedia,
        pokemon
    };

        let authUpdated = false;
        let userUpdated = false;


        //Routes to update information to 
        //Routes split due to different information in different microservices
        if (updatedPassword || updateFullName) {
        await addAuthMutation.mutateAsync(authUpdateData);
        authUpdated = true;
        }

        if (profileIconColor || bio || gender) {
        await addUserMutation.mutateAsync(userUpdateData);
        userUpdated = true;
        }

        if(!authUpdateData && !userUpdateData){
            toast.error("No changes have been made");
        }
    };

  return (
    <div>
        {isLoggedIn ? (
            <PageWrapper>
                {isLoading ? (
                    <p>Loading...</p>
                ) : (
                    <Wrapper onSubmit={handleUpdate} id="updateform">
                        <LabelGroup>
                                <Label htmlFor="username" text="Username"/>
                                <DisplayText>{username}</DisplayText>
                        </LabelGroup> 

                        <LabelGroup>
                                <Label htmlFor="email" text="Email"/>
                                <DisplayText>{currentEmail.email}</DisplayText>
                        </LabelGroup>

                        <LabelGroup>
                                <Label htmlFor='dateofbirth' text="Date of Birth"/>
                                <DisplayText>{currentDob.dateOfBirth}</DisplayText>
                        </LabelGroup>

                        <LabelGroup>
                            <Label htmlFor="gender" text="Gender" />
                            <Select name="gender" value={curGender.gender} onChange={(e) => setGender(e.target.value)}>
                            <option>Select a Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="Other">Other</option>
                            </Select>
                        </LabelGroup>

                        <LabelGroup>
                                <Label htmlFor="password" text="Update Password"/>
                                <Input 
                                    type="password"
                                    placeholder="Please enter a password"
                                    value={updatedPassword}
                                    onValueChange={setUpdatedPassword}
                                    left="2.5%"
                                />
                                {errors.updatedPassword && <Error>{errors.updatedPassword}</Error>}
                        </LabelGroup>

                        <LabelGroup>
                                <Label htmlFor="password" text="Retype Password"/>
                                <Input 
                                    type="password"
                                    placeholder="Please enter your password again"
                                    value={passwordRetype}
                                    onValueChange={setPasswordRetype}
                                    left="2.5%"
                                />
                                {errors.passwordRetype && <Error>{errors.passwordRetype}</Error>}
                        </LabelGroup>

                        <LabelGroup>
                                <Label htmlFor="fullName" text="Full Name"/>
                                <DisplayText>{fullname.fullName}</DisplayText>
                                <Input 
                                    type="text"
                                    placeholder="Please enter your name"
                                    value={fullname.fullName}
                                    onValueChange={setUpdateFullName}
                                    left="2.5%"
                                />
                                {errors.updateFullName && <Error>{errors.updateFullName}</Error>}
                        </LabelGroup>

                        <LabelGroup>
                          <Label htmlFor="twitter" text={<BsTwitter/>} />
                          <Input
                            type="text"
                            placeholder="Twitter profile URL"
                            value={socials.socials.twitter}
                            onValueChange={(value) => setSocialMedia({ ...socialMedia, twitter: value })}
                            left="2.5%"
                          />
                        </LabelGroup>

                        <LabelGroup>
                          <Label htmlFor="facebook" text={<BsFacebook/>} />
                          <Input
                            type="text"
                            placeholder="Facebook profile URL"
                            value={socials.socials.facebook}
                            onValueChange={(value) => setSocialMedia({ ...socialMedia, facebook: value })}
                            left="2.5%"
                          />
                        </LabelGroup>

                        <LabelGroup>
                          <Label htmlFor="instagram" text={<BsInstagram/>} />
                          <Input
                            type="text"
                            placeholder="Instagram profile URL"
                            value={socials.socials.instagram}
                            onValueChange={(value) => setSocialMedia({ ...socialMedia, instagram: value })}
                            left="2.5%"
                          />
                        </LabelGroup>

                        <LabelGroup>
                          <Label htmlFor="interests" text="Interests" />
                          <DisplayText>{currentInterests.interests}</DisplayText>
                          <TextArea
                            type="text"
                            placeholder="Your interests"
                            value={currentInterests.interests.join(", ")}
                            onValueChange={(value) => setInterests(value.split(",").map((item) => item.trim()))}
                            width="90%"
                          />
                        </LabelGroup>

                        <LabelGroup>
                                <Label htmlFor="bio" text="Bio" />
                                <DisplayText>{currentBio.bio}</DisplayText>
                                <TextArea
                                    type="text"
                                    placeholder="Bio"
                                    value={bio}
                                    onValueChange={setBio}
                                    width="90%"
                                />
                        </LabelGroup>

                        <LabelGroup>
                            <Label htmlFor="profileIconColor" text="Profile Icon Color" />
                            <TwitterPicker
                            color={profileIconColor}
                            onChange={(color) => setProfileIconColor(color.hex)}
                            className={css`
                                margin-top:20px;
                                margin-left:3%;
                                padding:15px;
                                @media (max-width: 770px) {
                                    margin-bottom:15px;
                                }
                            `}
                            />
                        </LabelGroup>
                        <PokemonCard/>
                        <div className={css`
                            width:20%;
                            position:absolute;
                            bottom:-60px;
                            margin-left:2.5%;
                            @media (max-width: 770px) {
                                width:50%;
                            }
                        `}>
                            <Submit value="Update Profile" form="updateform"/>
                        </div>
                        <ToastContainer/>
                    </Wrapper>
                )}
            </PageWrapper>
        ) 
        : (navigate('../login'))}
    </div>
  )
}

export default EditProfile